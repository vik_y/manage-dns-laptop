#!/usr/bin/python3
from shell import Shell
import os 
import time
import logging 
import logging.config
import yaml

with open('logging.yaml', 'r') as f:
    conf = yaml.load(f)

logging.config.dictConfig(conf)
#logger = logging.getLogger('myapp')
logger = logging.getLogger()
sh = Shell() 

default = '1.1.1.1'

# Test if a dns server is reachable and set it 
def test_and_set(server):
    logger.info('Testing server:' + server)
    cmd = f'ping -c 1 -W 1 {server}'
    result = sh.run(cmd)

    server_available = sh.run(f'ping -c 1 -W 1 {server}') 
    
    # Dont waste anymore time if server is not available
    if server_available.code !=0:
        return False 
    
    server_has_dns = sh.run(f'dig @{server} +timeout=2 google.com') 
    
    if server_has_dns.code == 0 and server_available.code == 0:
        logger.info(f'{server} Reachable')
        logger.info(f'Setting dns server: {server}')
        os.system(f'echo nameserver {server} > /etc/resolv.conf')
        return True
    else:
        logger.info("PING", server_available.output())
        logger.info("DIG", server_has_dns.output())
        return False



while 1:
    # Check if office dns reachable 
    if test_and_set('172.16.211.16'):
        logger.info('Office DNS Reachable')
        time.sleep(10)
        continue
    
    if test_and_set('192.168.0.102'):
        logger.info('Home DNS Reachable')
        time.sleep(10)
        continue

    # Check if local dns is working fine or not 
    cmd = sh.run('dig @127.0.0.53 +timeout=2 google.com')
    if cmd.code == 0:
        logger.info("Switching to 127.0.0.53")
        logger.info(cmd.output())
        os.system('echo "nameserver 127.0.0.53" > /etc/resolv.conf')
    else:
        logger.info(f'Switching to {default}')
        os.system(f'echo "nameserver {default}" > /etc/resolv.conf')
        
    time.sleep(1) # sleep for a second

